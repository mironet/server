package server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/server/check"

	_ "net/http/pprof"
)

const (
	defaultAddress         = ":8080"
	defaultMetricsPath     = "/metrics"
	defaultDebugPathPrefix = "/debug/"
	defaultLogLevel        = logrus.InfoLevel

	defaultReadTimeout  = 5 * time.Second
	defaultWriteTimeout = 10 * time.Second
	defaultIdleTimeout  = 2 * time.Minute
)

type Opt func(s *Server) error

func defaultOpts() ([]Opt, error) {
	immediateSuccess := func() error { return nil }
	immediateOK, err := check.New("immediate OK", immediateSuccess, check.WithStatus(check.Result{Status: check.StatusPass}))
	if err != nil {
		return nil, fmt.Errorf("could not get immediate OK check: %w", err)
	}
	return []Opt{
		WithAddress(defaultAddress),
		WithTimeouts(defaultReadTimeout, defaultWriteTimeout, defaultIdleTimeout),
		WithLogrusLogWriter(defaultLogLevel),
		WithReadinessCheck(immediateOK),
		WithHealthCheck(immediateOK),
		WithLivenessCheck(immediateOK),
		WithMetricsPath(defaultMetricsPath),
	}, nil
}

func WithAddress(address string) Opt {
	return func(s *Server) error {
		s.address = address
		return nil
	}
}

func WithLogrusLogWriter(level logrus.Level) Opt {
	return func(s *Server) error {
		s.Logger = NewLogrusLogger(level)
		return nil
	}
}

func WithPprofEndpoint(pathPrefix string) Opt {
	return func(s *Server) error {
		if pathPrefix == "" {
			pathPrefix = defaultDebugPathPrefix
		}

		s.Router.PathPrefix(pathPrefix).Handler(http.DefaultServeMux)
		return nil
	}
}

func WithHealthCheck(c *check.Check) Opt {
	return func(s *Server) error {
		s.healthCheck = c
		return nil
	}
}

func WithReadinessCheck(c *check.Check) Opt {
	return func(s *Server) error {
		s.readinessCheck = c
		return nil
	}
}

func WithLivenessCheck(c *check.Check) Opt {
	return func(s *Server) error {
		s.livenessCheck = c
		return nil
	}
}

func WithTimeouts(read, write, idle time.Duration) Opt {
	return func(s *Server) error {
		s.readTimeout = read
		s.writeTimeout = write
		s.idleTimeout = idle
		return nil
	}
}

func WithMetricsPath(path string) Opt {
	return func(s *Server) error {
		if path == "" {
			path = defaultMetricsPath
		}

		s.metricsPath = path
		return nil
	}
}
