
# Getting started

## Minimal example

Here is a minimal example that shows how to use the mironet server:

```go
package main

import (
  "fmt"
  "net/http"

  "gitlab.com/mironet/server"
)

func main() {
  // Get new default server. Also add a pprof debugging endpoint.
  s, err := server.NewServer(server.WithPprofEndpoint("/debug/"))
  if err != nil {
    return fmt.Errorf("could not get a new server: %w", err)
  }

  // Add custom endpoints.
  s.Router.HandleFunc("/my/favorite/func", func(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("hello world"))
  })

  // Run the server. Note: Run() is a blocking call.
  err := s.Run(gCtx)
  if err != nil {
    return fmt.Errorf("error during server run: %w", err)
  }
  return nil
}
```

## Default options

If you start the server without options, it will

* listen on port `:8080`
* have default http read timeout of `5` seconds
* have default http write timeout of `10` seconds
* have default http idle timeout of `2` minutes
* log stuff using a `logrus` logger with log level `debug`
* offer `/livez` endpoint
* offer `/readyz` endpoint
* offer `/healthz` endpoint
* offer `/metrics` endpoint with default prometheus metrics

One can change these defaults by handing over various `server.With<Opt>` methods
when creating the server.
