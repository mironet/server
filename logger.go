package server

import (
	"fmt"
	"io"
)

type Logger interface {
	// Returns an io.Writer that can be used to write to the standard log level.
	// It is the callers responsibility to close the writer when done.
	LogWriter() io.WriteCloser
	// Writes a formated string on the standard log level.
	Logf(format string, args ...interface{}) error
	// Writes a line containing the given string on the standard log level.
	LogLn(s string) error
	// Writes a formated string on the info log level.
	Infof(format string, args ...interface{})
	// Writes a formated string on the debug log level.
	Debugf(format string, args ...interface{})
	// Writes a formated string on the warn log level.
	Warnf(format string, args ...interface{})
	// Writes a formated string on the error log level.
	Errorf(format string, args ...interface{})
	// Writes a formated string on the trace log level.
	Tracef(format string, args ...interface{})
}

func logf(l Logger, format string, args ...interface{}) error {
	if l == nil {
		return fmt.Errorf("logger cannot be nil")
	}
	log := fmt.Sprintf(format, args...)
	return l.LogLn(log)
}

func logLn(l Logger, log string) error {
	if l == nil {
		return fmt.Errorf("logger cannot be nil")
	}
	logWriter := l.LogWriter()
	defer logWriter.Close()
	if logWriter == nil {
		return fmt.Errorf("log writer is nil")
	}
	bytes := []byte(log)
	_, err := logWriter.Write(bytes)
	if err != nil {
		return fmt.Errorf("could not write to log writer: %v", err)
	}
	_, err = logWriter.Write([]byte{'\n'})
	if err != nil {
		return fmt.Errorf("could not write to log writer: %v", err)
	}
	return nil
}
