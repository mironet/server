package server

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/mironet/server/check"
	"golang.org/x/sync/errgroup"
)

type Server struct {
	Logger
	srv            *http.Server
	Router         *mux.Router
	address        string
	healthCheck    *check.Check
	readinessCheck *check.Check
	livenessCheck  *check.Check
	writeTimeout   time.Duration
	readTimeout    time.Duration
	idleTimeout    time.Duration
	metricsPath    string
}

func NewServer(opts ...Opt) (*Server, error) {
	server := &Server{
		Router: mux.NewRouter(),
	}

	// Establish the default server configuration.
	defaultOpts, err := defaultOpts()
	if err != nil {
		return nil, fmt.Errorf("could not get default options: %w", err)
	}
	for _, opt := range defaultOpts {
		if err := opt(server); err != nil {
			return nil, err
		}
	}

	// Diverge from standard server configuration applying user provided
	// options.
	for _, opt := range opts {
		if err := opt(server); err != nil {
			return nil, err
		}
	}

	server.Router.HandleFunc("/healthz", server.statusHandler(server.healthCheck)).Methods(http.MethodGet)
	server.Router.HandleFunc("/readyz", server.statusHandler(server.readinessCheck)).Methods(http.MethodGet)
	server.Router.HandleFunc("/livez", server.statusHandler(server.livenessCheck)).Methods(http.MethodGet)
	server.Router.Handle(server.metricsPath, promhttp.Handler())

	server.srv = &http.Server{
		Handler:      handlers.LoggingHandler(server.LogWriter(), server.Router),
		Addr:         server.address,
		WriteTimeout: server.writeTimeout,
		ReadTimeout:  server.readTimeout,
		IdleTimeout:  server.idleTimeout,
	}

	return server, nil
}

type resulter interface {
	Result() check.Result
}

func (s *Server) statusHandler(resulter resulter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var buf bytes.Buffer
		msg := resulter.Result()
		if err := json.NewEncoder(&buf).Encode(msg); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			s.LogLn(fmt.Sprintf("error encoding message: %s", err))
			return
		}
		w.Header().Set("Content-Type", check.ContentType)
		if msg.Status != check.StatusPass {
			w.WriteHeader(http.StatusServiceUnavailable)
		}
		if _, err := io.Copy(w, &buf); err != nil {
			s.LogLn(fmt.Sprintf("error sending error to client: %s", err))
		}
	}
}

func (s *Server) Run(ctx context.Context) error {
	// Ensure runCtx is marked done, when ctx is done, stop is called, an
	// os.Interrupt arrives or a syscall.SIGTERM arrives, whichever happens
	// first.
	runCtx, stop := signal.NotifyContext(ctx, os.Interrupt, syscall.SIGTERM)
	defer stop()

	// Start all check routines (asynchronously). The check routines are written
	// such that they terminate once the given context is cancelled.
	healthDoneChan := s.healthCheck.Run(runCtx)
	readyDoneChan := s.readinessCheck.Run(runCtx)
	liveDoneChan := s.livenessCheck.Run(runCtx)

	s.srv.BaseContext = func(_ net.Listener) context.Context {
		return runCtx
	}

	// Create a new error group and an associated context derived from runCtx.
	g, gCtx := errgroup.WithContext(runCtx)

	// Start a routine that waits for all the check routines to be terminated.
	g.Go(func() error {
		s.LogLn("starting check routines")
		defer s.LogLn("check routines terminated")
		numDones := 0
		for numDones < 3 {
			var ok bool
			select {
			case _, ok = <-healthDoneChan:
			case _, ok = <-readyDoneChan:
			case _, ok = <-liveDoneChan:
			case <-gCtx.Done():
				return ctx.Err()
			}
			if !ok {
				continue
			}
			numDones = numDones + 1
		}
		return nil
	})

	// Start the actual server routine.
	g.Go(func() error {
		s.LogLn(fmt.Sprintf("starting server listening on %s", s.address))
		defer s.LogLn("server terminated")
		return s.srv.ListenAndServe()
	})

	// Start the shutdown routine. It waits for the gCtx to be done before it
	// shuts down the server. Note: In our case this means, it waits until
	// ListenAndServe() returns a non-nil error or the runCtx is done, whichever
	// happens first.
	g.Go(func() error {
		<-gCtx.Done()
		s.LogLn("shutting down server")
		defer s.LogLn("shut down terminated")
		return s.srv.Shutdown(ctx)
	})

	return g.Wait()
}
