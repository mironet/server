package server

import (
	"io"

	"github.com/sirupsen/logrus"
)

type logrusLogger struct {
	*logrus.Logger
}

func NewLogrusLogger(level logrus.Level) Logger {
	l := logrus.New()
	l.SetLevel(level)
	return &logrusLogger{
		Logger: l,
	}
}

// Returns an io.Writer that can be used to write to the standard log level. It
// is the callers responsibility to close the writer when done.
func (l *logrusLogger) LogWriter() io.WriteCloser {
	return l.Writer()
}

// Writes a formated string on the standard log level.
func (l *logrusLogger) Logf(format string, args ...interface{}) error {
	return logf(l, format, args...)
}

// Writes a line containing the given string on the standard log level.
func (l *logrusLogger) LogLn(s string) error {
	return logLn(l, s)
}
